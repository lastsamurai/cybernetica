from flask import Flask, request, render_template, make_response
from functools import wraps
import socket
app = Flask(__name__)


def auth_required(f):
	@wraps(f)
	def decorated(*args,**kwargs):

		db = dict()
		db['username']='password'

		auth=request.authorization
		if auth and (auth.username in db) and auth.password==db[auth.username]:
			return f(*args,**kwargs)

		return make_response('Could not verify your login!',401,{'WWW-Authenticate':'Basic realm="Login Required"'})
	return decorated


@app.route("/")
def index():

	endpoints=[{"title":"ServerName","url":"/Servername"},{"title":"IPAddress","url":"/IPAddress"}]
	message=False

	if not request.authorization:
		message= "You are currently NOT using HTTP Basic Authentication; You need it for the following endpoints"

	return render_template('index.html',endpoints=endpoints,message=message);

@app.route("/Servername")
@auth_required
def servername():
	return socket.gethostname()

@app.route("/IPAddress")
@auth_required
def ipaddress():
    return socket.gethostbyname(socket.gethostname())

if __name__ =="__main__":
    app.run()
    #app.run(ssl_context='adhoc')
